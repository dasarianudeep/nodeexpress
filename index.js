const express = require('express');
const app = express();

app.get('/', function (req, res) {
    res.send('<strong>Hello Node Express</strong>');
})

app.listen(3000, function () {
    console.log('Server listening at port - 3000');
});